# referred from from frappe.core.doctype.data_import.data_import
def post_process_doc_json(
    docs=[],
    del_parent_keys=(),
    del_child_keys=(),
    replace_parent_keys={},
    replace_child_keys={},
    defaults_parent={},
    defaults_child={},
    default_child_delete_keys=("docstatus", "doctype", "modified", "name"),
    common_del_keys=("modified_by", "creation", "owner", "idx", "lft", "rgt"),
):
    """

    Note on Tree DocTypes:
        The tree structure is maintained in the database via the fields "lft"
        and "rgt". They are automatically set and kept up-to-date. Importing
        them would destroy any existing tree structure. For this reason they
        are not exported as well.

    args:
        - docs (list):
            Array of documents to be processed

        - del_parent_keys (tuple, optional):
            Tuple of keys to be deleted from parent doc

        - del_child_keys (tuple, optional):
            Tuple of keys to be deleted from child doc

        - replace_parent_keys (dict, optional):
            Mapping of keys to be replaced in parent doc

        - replace_child_keys (dict, optional):
            Mapping of keys to be replaced in child doc
                Eg: replace_parent_keys = {
                    "replace_from_key": "replace_to_key",
                    }

        - defaults_parent (dict, optional):
            Add fields to parent doc

        - defaults_child (dict, optional):
            Add fields to child doc

    """

    del_parent_keys = common_del_keys + del_parent_keys
    del_child_keys = (
        common_del_keys + default_child_delete_keys + del_child_keys  # noqa: 501
    )

    def replace_fields(doc, keys_map):
        # Replaces keys in the document based on the provided mapping
        for replace_from, replace_to in keys_map.items():
            if replace_from in doc:
                doc[replace_to] = doc.pop(replace_from, "")

    def add_default_fields(doc, values):
        # Add field values in the document
        for k, v in values.items():
            doc[k] = v

    for doc in docs:
        # Replace keys in the parent document
        replace_fields(doc, replace_parent_keys)

        # Add default fields in parent document
        add_default_fields(doc, defaults_parent)

        # Delete specified keys from the parent document
        for key in del_parent_keys:
            if key in doc:
                del doc[key]

        for k, v in doc.items():
            if isinstance(v, list):
                # Process child documents in the list
                for child in v:
                    # Replace keys in the child document
                    replace_fields(child, replace_child_keys)

                    # Add default fields in child document
                    add_default_fields(child, defaults_child)

                    # Delete specified keys from the child document
                    for key in del_child_keys:
                        if key in child:
                            del child[key]


def post_process_child_tables(doctype, doc_json):
    """
    Compare doctype json child tables with doc_json

    if row is added:
        - make islocal = 1
        - delete creation

    if row is updated:
        - copy creation date from doctype json
    """

    child_field_names = [
        "fields",
        "permissions",
        "actions",
        "links",
        "states",
    ]
    for field_name in child_field_names:
        for child in doc_json.get(field_name):

            existing_field = [
                field
                for field in doctype.get(field_name)
                if field.get("name") == child.get("name")
            ]

            # modified date for child tables gets auto-updated internally
            # check `set_user_and_timestamp` in document.py
            child["modified"] = None

            if existing_field:
                child["creation"] = stringyfy_date(
                    existing_field[0].get("creation")
                )  # noqa: 501

            else:
                child["__islocal"] = 1
                child["creation"] = None


def stringyfy_date(date, custom_format="%Y-%m-%d %H:%M:%S.%f"):
    return date.strftime(custom_format)
