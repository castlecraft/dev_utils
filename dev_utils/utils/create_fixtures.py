import json
import os

import frappe
from frappe.core.doctype.data_import.data_import import export_json
from frappe.utils import now


@frappe.whitelist()
def create_fixtures(master_meta_name, exported_data_filter='{"filters": []}'):
    if isinstance(exported_data_filter, str):
        exported_data_filter = json.loads(exported_data_filter)
    master_meta = frappe.get_doc("Master Meta", master_meta_name)
    export_fixtures(master_meta, exported_data_filter)

    frappe.db.set_value("Master Meta", master_meta_name, "status", "Synced")
    frappe.db.commit()


def export_fixtures(master_meta, exported_data_filter):
    dir_path = frappe.get_app_path(
        master_meta.exported_app,
        "masters",
        frappe.scrub(master_meta.meta_doctype),  # noqa: 501
    )

    create_directory(dir_path)

    export_json(
        master_meta.meta_doctype,
        dir_path + "/data.json",
        filters=exported_data_filter.get("filters"),
    )

    exclude_update = [
        fields.field_name
        for fields in master_meta.exclude_update
        if len(master_meta.exclude_update)
    ]

    exclude_import = [
        fields.field_name
        for fields in master_meta.exclude_import
        if len(master_meta.exclude_import)
    ]

    payload = get_file_content(dir_path + "/meta.json")

    if payload:
        # update version
        version = payload.get("version")
        payload["version"] = version + 1 if version else 1

        # update exclude_update
        payload["exclude_update"] = exclude_update

        # update exclude_update
        payload["exclude_import"] = exclude_import

        # update latest_modified
        payload["latest_modified"] = now()
        payload["app_name"]: master_meta.doctype_owner

        with open(dir_path + "/meta.json", "w") as outfile:
            outfile.write(frappe.as_json(payload))

    else:
        # create payload
        payload = {
            "exclude_update": exclude_update,
            "exclude_import": exclude_import,
            "version": 1,
            "latest_modified": now(),
            "app_name": master_meta.doctype_owner,
        }

        with open(dir_path + "/meta.json", "w") as outfile:
            outfile.write(frappe.as_json(payload))


def create_directory(path):
    if not os.path.exists(path):
        os.makedirs(path)


def get_file_content(path):
    if not os.path.exists(path):
        return None
    else:
        with open(path) as outfile:
            data = json.load(outfile)
            return data
