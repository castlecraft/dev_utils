import os
import shutil

import frappe

from dev_utils.utils.create_fixtures import create_fixtures


def after_delete_master_meta(self):
    if self.doctype == "Master Meta":
        # NOTE: For cases where existing exported app has broken
        #       meta, these docs cannot be deleted and any file
        #       cleanup will have to be done manually
        if not self.exported_app:
            return

        dir_path = frappe.get_app_path(
            self.exported_app,
            "masters",
            frappe.scrub(self.meta_doctype),  # noqa: 501
        )

        if os.path.exists(dir_path):
            shutil.rmtree(dir_path)
        return

    if frappe.db.exists("Master Meta", self.doctype):
        master_meta = frappe.get_cached_doc("Master Meta", self.doctype)

        if master_meta.auto_sync_fixture:
            create_fixtures(
                master_meta.name,
                master_meta.exported_data_filters,
            )

        else:
            master_meta.status = "Desynced"
            master_meta.save(ignore_permissions=True)
