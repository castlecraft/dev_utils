import frappe

from dev_utils.utils.create_fixtures import create_fixtures


def update_master_meta(self, operation="update"):
    if self.doctype == "Master Meta" and operation == "delete":
        return

    if self.doctype == "Master Meta":
        if self.auto_sync_fixture:
            create_fixtures(self.name, self.exported_data_filters)

        return

    if frappe.db.exists("Master Meta", self.doctype):
        master_meta = frappe.get_cached_doc("Master Meta", self.doctype)

        if master_meta.auto_sync_fixture:
            create_fixtures(
                master_meta.name,
                master_meta.exported_data_filters,
            )

        else:
            master_meta.status = "Desynced"
            master_meta.save(ignore_permissions=True)
