from dev_utils.common.decorators import exclude_doc_event_hooks
from dev_utils.master_meta.update_master_meta import update_master_meta

from dev_utils.master_meta.after_delete_master_meta import (  # isort: skip
    after_delete_master_meta,
)  # noqa: E501


@exclude_doc_event_hooks()
def after_delete(self, method=None):
    if method is None:
        return True
    after_delete_master_meta(self)
    update_master_meta(self, operation="delete")
