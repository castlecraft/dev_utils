from dev_utils.common.decorators import exclude_doc_event_hooks
from dev_utils.master_meta.update_master_meta import update_master_meta


@exclude_doc_event_hooks()
def on_update(self, method=None):
    if method is None:
        return True

    update_master_meta(self)
