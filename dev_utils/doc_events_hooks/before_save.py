from dev_utils.common.decorators import exclude_doc_event_hooks
from dev_utils.dev_utils.doctype.before_save_doctype import before_save_doctype


@exclude_doc_event_hooks()
def before_save(self, method=None):
    if method is None:
        return True

    before_save_doctype(self)
