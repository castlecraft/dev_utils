import re

import frappe
import inflect
from frappe import _

from dev_utils.common.inflect_helper import is_singular

SPECIAL_CHAR_PATTERN = re.compile(r"[\W][\s]", flags=re.UNICODE)


class ValidateDoctypeEditor:
    error = []

    def __init__(self, doctype_editor):
        self.error.clear()
        self.inflect = inflect.engine()
        self.doctype_editor = doctype_editor
        self.validate_doctype_name()
        self.validate_field_spaces()
        self.validate_child_field_spaces()
        self.validate_child_field_label_length()
        self.validate_child_field_label_name()
        self.validate_child_field_name_length()
        self.validate_child_field_name()
        self.validate_child_field_length()
        self.validate_link_table_options()
        self.validate_child_fieldtype()

    def is_new(self):
        return self.doctype_editor.get("__islocal")

    def validate_doctype_name(self):
        if not self.doctype_editor.get("create_new"):
            return

        doctype_name = self.doctype_editor.get("doctype_name")

        # Validating doctype name length
        if not self.doctype_editor.get("istable"):
            if len(doctype_name) > 48:
                self.error.append(
                    {
                        "identifier": "New Doctype Name",
                        "message": f""" Check length of <b>
                                {doctype_name} </b>.
                                <br>
                                Doctype name length
                                should not <b>exceed</b>
                                more than <b>48 characters</b>. """,
                    }
                )

        # Validating name case convention
        if doctype_name != doctype_name.title():
            self.error.append(
                {
                    "identifier": "New Doctype Name",
                    "message": f""" Doctype <b>{doctype_name}</b> name case
                            is not correct.
                            Follow <b>title name case convention</b>,
                            for eg: 'Sales Order', 'Item Invoice'. """,
                }
            )

        # Singular validation
        if not is_singular(self.inflect, doctype_name):
            self.error.append(
                {
                    "identifier": "New Doctype Name",
                    "message": f""" Doctype name should be <b>singular</b>
                        eg: {self.inflect.singular_noun(doctype_name)}. """,
                }
            )

    def validate_field_spaces(self):
        to_validate_fields = (
            ["from_doctype"]
            if not self.doctype_editor.get("create_new")
            else ["doctype_name"]
        )

        for field in to_validate_fields:
            phrase = self.doctype_editor.get(field)
            self.validate_spaces_between_phrase(phrase, field)

    def validate_child_field_spaces(self):
        for child_fields in self.doctype_editor.get("fields") or []:
            phrase = child_fields.get("label")
            self.validate_spaces_between_phrase(
                phrase, "index: " + str(child_fields.get("idx")) + ", "
            )

    def validate_spaces_between_phrase(self, phrase, identifier=None):
        if not phrase:
            return

        spaces = phrase.split(" ")

        if "" in spaces:
            self.error.append(
                {
                    "identifier": f""" {identifier} """,
                    "message": (
                        _(
                            f""" Check phrase <b>'{phrase}'</b> for {identifier} it has extra spaces. """  # noqa: E501
                        )
                    ),
                }
            )

    def validate_child_field_label_length(self):
        for field in self.doctype_editor.get("fields") or []:
            if not field.get("label"):
                continue

            length = len(field.get("label"))

            if length > 48:
                self.error.append(
                    {
                        "identifier": field,
                        "message": f""" Check length of <b>
                                {field.get("label")}</b> in field Label. <br>
                                Label length should not <b>exceed</b> more
                                than <b> 48 characters</b>. """,
                    }
                )

    def validate_child_field_name_length(self):
        for field in self.doctype_editor.get("fields") or []:
            if not field.get("fieldname"):
                continue

            length = len(field.get("fieldname"))

            if length > 30:

                self.error.append(
                    {
                        "identifier": field,
                        "message": f""" Check length of <b>
                                {field.get("fieldname")} </b> in field Name.
                                <br>
                                Fieldname length should not <b>exceed</b>
                                more than <b>30 characters</b>. """,
                    }
                )

    def validate_child_field_name(self):
        for field in self.doctype_editor.get("fields") or []:
            fieldname = field.get("fieldname")

            if not fieldname:
                self.error.append(
                    {
                        "identifier": field,
                        "message": f""" Field label
                                on row {field.get("idx")} cannot be blank
                                .""",
                    }
                )
                continue

            if special_characters := SPECIAL_CHAR_PATTERN.findall(
                fieldname  # noqa: E501
            ):
                special_characters = ", ".join(
                    f'"{c}"' for c in special_characters  # noqa: E501
                )

                self.error.append(
                    {
                        "identifier": field,
                        "message": f""" Field Name
                                <b> {fieldname}</b>
                                on row {field.get("idx")} cannot have a
                                special characters like
                                <b>{special_characters}</b>.""",
                    }
                )

            # Plural validation
            if field.get("fieldtype") == "Table":
                if is_singular(self.inflect, fieldname):
                    self.error.append(
                        {
                            "identifier": field,
                            "message": f""" Field Name
                                    <b> {fieldname}</b>
                                    on row {field.get("idx")}
                                    for type {field.get("fieldtype")}
                                    should be plural.""",
                        }
                    )

    def validate_child_field_label_name(self):
        for field in self.doctype_editor.get("fields") or []:
            if not field.get("label"):
                self.error.append(
                    {
                        "identifier": field,
                        "message": f""" Field label
                                on row {field.get("idx")} cannot be blank
                                .""",
                    }
                )
                continue

            if special_characters := SPECIAL_CHAR_PATTERN.findall(
                field.get("label")  # noqa: E501
            ):
                special_characters = ", ".join(
                    f'"{c}"' for c in special_characters  # noqa: E501
                )

                self.error.append(
                    {
                        "identifier": field,
                        "message": f""" Field label
                                <b> {field.get("label")}</b>
                                on row {field.get("idx")} cannot have a
                                special characters like
                                <b>{special_characters}</b>.""",
                    }
                )

    def validate_child_field_length(self):
        fields = self.doctype_editor.get("fields")

        validate_length_for_fields = [
            "Data",
            "Select",
            "Read Only",
            "Attach",
            "Attach Image",
            "Int",
        ]

        for field in fields:
            if field.get("fieldtype") in validate_length_for_fields:
                if not field.get("length"):
                    self.error.append(
                        {
                            "identifier": field,
                            "message": f""" Length should be set for
                                fieldtype <b>{field.get("fieldtype")}</b> at
                                <b>row {field.get("idx")}</b>. """,
                        }
                    )

    def validate_link_table_options(self):
        for d in self.doctype_editor.get("fields"):
            if frappe.flags.in_patch or frappe.flags.in_fixtures:
                return

            if d.get("fieldtype") == "Table":
                options = d.get("options")
                if options != d.get("parent"):
                    doctype = self.doctype_editor.get(
                        "doctype_name"
                    ) or self.doctype_editor.get("from_doctype")
                    if (not options) or (
                        not options.startswith(doctype + " ")
                    ):  # noqa: E501
                        self.error.append(
                            {
                                "identifier": options,
                                "message": "Child table name must be <b>Parent</b> + <b>Relation</b>.",  # noqa: E501
                            }
                        )

    def validate_child_fieldtype(self):
        if self.is_new():
            return

        if self.doctype_editor.get("from_doctype"):
            doctype = frappe.get_doc(
                "DocType", self.doctype_editor.get("from_doctype")
            )  # noqa: E501

            docFieldsName = {}
            for docfield in doctype.fields:
                docFieldsName[docfield.name] = docfield

            for docEditfieldtype in self.doctype_editor.get("fields"):
                if docEditfieldtype.get("reference_doctype_name"):
                    docfieldtype = docFieldsName.get(
                        docEditfieldtype.get("reference_doctype_name")
                    )

                    if docfieldtype.get("fieldtype") != docEditfieldtype.get(
                        "fieldtype"
                    ):
                        self.error.append(
                            {
                                "identifier": docEditfieldtype,
                                "message": f"""
                                    <span style="color: red;">
                                        The field type of <b>row {docEditfieldtype.get("idx")}</b> is being changed from <b>{docfieldtype.get("fieldtype")}</b> to <b>{docEditfieldtype.get("fieldtype")}</b>, which may cause unexpected errors.
                                    </span>""",  # noqa: E501
                            }
                        )
