// Copyright (c) 2024, prafful@castlecraft.in and contributors
// For license information, please see license.txt
/* global frappe, $, __ */

const defaultFields = [
  'doctype',
  'name',
  'owner',
  'creation',
  'modified',
  'modified_by',
  'docstatus',
  'idx',
];
const fieldMappings = {
  fields: ['fieldtype', 'fieldname', 'length'],
  permissions: ['role', 'permlevel'],
  actions: ['label', 'action_type', 'action'],
  links: ['link_doctype', 'link_fieldname'],
  states: ['title', 'color'],
};
const childTablesProperty = [];

function filterChildTableProperty(doctypeName) {
  frappe.model.with_doctype('DocField Editor', () => {
    $.map(frappe.get_doc('DocType', doctypeName).fields, d => {
      if (
        frappe.model.no_value_type.includes(d.fieldtype) &&
        !frappe.model.table_fields.includes(d.fieldtype)
      ) {
        return null;
      }
      if (
        d.fieldname !== undefined &&
        !childTablesProperty.includes(d.fieldname)
      ) {
        childTablesProperty.push(d.fieldname);
      }
    });
  });
}

frappe.call({
  method: 'frappe.client.get',
  args: {
    doctype: 'DocType',
    name: 'DocType Editor',
  },
  callback(res) {
    if (res.message) {
      const docTypeEditor = res.message;

      for (const field of docTypeEditor.fields) {
        if (field.fieldtype === 'Table') {
          filterChildTableProperty(field.options);
        }
      }
    }
  },
});

function appendFieldChanges(frm, field, doctypeDoc, doctypeEditorDoc) {
  // value should not be in "array" or "object"
  if (
    typeof doctypeEditorDoc[field] !== 'object' &&
    !(doctypeEditorDoc[field] instanceof Array)
  ) {
    // Check if the field had no value before (null) and the new value is not empty to avoid comparing null with an empty string.
    if (doctypeDoc[field] === null && doctypeEditorDoc[field] !== '') {
      if (doctypeDoc[field] !== doctypeEditorDoc[field]) {
        frm.add_child('doctype_fields', {
          property: field,
          original_value: doctypeDoc[field],
          new_value: doctypeEditorDoc[field],
        });
      }
    } else if (
      doctypeDoc[field] !== null &&
      doctypeDoc[field] !== doctypeEditorDoc[field]
    ) {
      frm.add_child('doctype_fields', {
        property: field,
        original_value: doctypeDoc[field],
        new_value: doctypeEditorDoc[field],
      });
    }
  }
}

function populateFieldDiff(frm, table, doctypeDoc, doctypeEditorDoc) {
  const doctypeDocObj = {};

  for (let i = 0; i < doctypeDoc[table].length; i++) {
    const oldField = doctypeDoc[table][i];
    // Assigning the field to doctypeDocObj using "name" as key
    doctypeDocObj[oldField.name] = oldField;
  }

  const doctypeEditorDocObj = {};
  let count = 0;

  for (let i = 0; i < doctypeEditorDoc[table].length; i++) {
    const newField = doctypeEditorDoc[table][i];
    if (newField.reference_doctype_name === null) {
      // Create a key for new field and assign the field as its value.
      count++;
      doctypeEditorDocObj[`new_field_${count}`] = newField;
    } else {
      // Assign the field to doctypeEditorDocObj using its "reference_doctype_name" as the key
      doctypeEditorDocObj[newField.reference_doctype_name] = newField;
    }
  }

  for (const prop in doctypeEditorDocObj) {
    if (doctypeDocObj[prop] !== undefined) {
      // Doc Fields Edited Changes
      for (const subProp in doctypeEditorDocObj[prop]) {
        if (childTablesProperty.includes(subProp)) {
          if (
            subProp !== 'reference_doctype_name' &&
            doctypeDocObj[prop][subProp] !== undefined
          ) {
            if (
              doctypeDocObj[prop][subProp] !==
              doctypeEditorDocObj[prop][subProp]
            ) {
              const parentfield = doctypeDocObj[prop].parentfield;
              const childTable = 'doc_' + parentfield + '_edited';

              if (childTable) {
                frm.add_child(childTable, {
                  row: doctypeEditorDocObj[prop].idx,
                  property: subProp,
                  original_value: doctypeDocObj[prop][subProp],
                  new_value: doctypeEditorDocObj[prop][subProp],
                });
              }
            }
          }
        }
      }

      // Doc Fields Moved Changes
      if (doctypeDocObj[prop].idx !== doctypeEditorDocObj[prop].idx) {
        const parentfield = doctypeEditorDocObj[prop].parentfield;
        const childTable = 'doc_' + parentfield + '_moved';

        if (childTable) {
          const diffObj = {
            old_position: doctypeDocObj[prop].idx,
            current_position: doctypeEditorDocObj[prop].idx,
            json_field: JSON.stringify(doctypeEditorDocObj[prop], null, 4),
          };

          const fields = fieldMappings[parentfield];
          if (fields) {
            fields.forEach(field => {
              diffObj[field] = doctypeEditorDocObj[prop][field];
            });
          }

          frm.add_child(childTable, diffObj);
        }
      }
    } else {
      // Doc Fields Created Changes
      const parentfield = doctypeEditorDocObj[prop].parentfield;
      const childTable = 'doc_' + parentfield + '_created';

      if (childTable) {
        const diffObj = {
          row: doctypeEditorDocObj[prop].idx,
          json_field: JSON.stringify(doctypeEditorDocObj[prop], null, 4),
        };

        const fields = fieldMappings[parentfield];
        if (fields) {
          fields.forEach(field => {
            diffObj[field] = doctypeEditorDocObj[prop][field];
          });
        }

        frm.add_child(childTable, diffObj);
      }
    }
  }

  // Doc Fields Deleted Changes
  for (const field in doctypeDocObj) {
    if (!Object.keys(doctypeEditorDocObj).includes(field)) {
      const parentfield = doctypeDocObj[field].parentfield;
      const childTable = 'doc_' + parentfield + '_deleted';

      if (childTable) {
        const diffObj = {
          row: doctypeDocObj[field].idx,
          json_field: JSON.stringify(doctypeDocObj[field], null, 4),
        };

        const fields = fieldMappings[parentfield];
        if (fields) {
          fields.forEach(fieldValue => {
            diffObj[fieldValue] = doctypeDocObj[field][fieldValue];
          });
        }

        frm.add_child(childTable, diffObj);
      }
    }
  }
}

function clearDoc(frm) {
  frm.doc.doctype_fields = [];

  frm.doc.doc_fields_edited = [];
  frm.doc.doc_fields_moved = [];
  frm.doc.doc_fields_created = [];
  frm.doc.doc_fields_deleted = [];

  frm.doc.doc_permissions_edited = [];
  frm.doc.doc_permissions_moved = [];
  frm.doc.doc_permissions_created = [];
  frm.doc.doc_permissions_deleted = [];

  frm.doc.doc_actions_edited = [];
  frm.doc.doc_actions_moved = [];
  frm.doc.doc_actions_created = [];
  frm.doc.doc_actions_deleted = [];

  frm.doc.doc_links_edited = [];
  frm.doc.doc_links_moved = [];
  frm.doc.doc_links_created = [];
  frm.doc.doc_links_deleted = [];

  frm.doc.doc_states_edited = [];
  frm.doc.doc_states_moved = [];
  frm.doc.doc_states_created = [];
  frm.doc.doc_states_deleted = [];

  frm.refresh_fields();
}

frappe.ui.form.on('DocType Editor Changes', {
  onload: frm => {
    frm.set_query('doctype_editor', () => {
      return {
        filters: {
          docstatus: 0,
          create_new: 0,
        },
      };
    });
    frm.page.set_primary_action(__('Save'), function () {}).addClass('hidden');
  },
  doctype_editor(frm) {
    clearDoc(frm);
    if (frm.doc.doctype_editor) {
      frappe.call({
        method: 'frappe.client.get',
        args: {
          doctype: 'DocType Editor',
          name: frm.doc.doctype_editor,
        },
        callback(response) {
          if (response.message) {
            const doctypeEditorDoc = response.message;
            const getDoctypeDoc = response.message.from_doctype;

            frappe.call({
              method: 'frappe.client.get',
              args: {
                doctype: 'DocType',
                name: getDoctypeDoc,
              },
              callback(res) {
                if (res.message) {
                  const doctypeDoc = res.message;
                  const childTablesFieldname = [];

                  for (const field in doctypeEditorDoc) {
                    if (!defaultFields.includes(field)) {
                      if (
                        Object.prototype.hasOwnProperty.call(
                          doctypeDoc,
                          field,
                        ) &&
                        Object.prototype.hasOwnProperty.call(
                          doctypeEditorDoc,
                          field,
                        )
                      ) {
                        // Doctype Fields Changes
                        appendFieldChanges(
                          frm,
                          field,
                          doctypeDoc,
                          doctypeEditorDoc,
                        );

                        if (Array.isArray(doctypeEditorDoc[field])) {
                          childTablesFieldname.push(field);
                        }
                      }
                    }
                  }
                  for (const table of childTablesFieldname) {
                    populateFieldDiff(frm, table, doctypeDoc, doctypeEditorDoc);
                  }
                  frm.refresh_fields();
                }
              },
            });
          }
        },
      });
    }
  },
});
