// Copyright (c) 2024, prafful@castlecraft.in and contributors
// For license information, please see license.txt
/* global frappe, $, locals, __ */

const fieldCheckList = [];
let fieldTypeList = [];

frappe.call({
  method: 'frappe.client.get',
  args: {
    doctype: 'DocType',
    name: 'DocField',
  },
  callback: r => {
    if (r.message) {
      const fieldOptions = r.message.fields
        .filter(e => e.fieldname === 'fieldtype')
        .map(e => e.options.split('\n'));

      // Flatten the list to get a single list of options
      fieldTypeList = fieldOptions.flat();
    }
  },
});

frappe.custom_provider = {
  set_fieldname_select: frm => {
    frappe.model.with_doctype('DocField Editor', () => {
      $.map(frappe.get_doc('DocType', 'DocField Editor').fields, d => {
        if (
          frappe.model.no_value_type.includes(d.fieldtype) &&
          !frappe.model.table_fields.includes(d.fieldtype)
        ) {
          return null;
        } else {
          if (
            ![undefined, 'docfield_master', 'is_docfield_master'].includes(
              d.fieldname,
            )
          ) {
            if (d.fieldtype === 'Check') {
              fieldCheckList.push(d.fieldname);
            }
            return d;
          }
        }
      });
    });
  },
};

function checkBoxValidation(fieldProperty, fieldValue) {
  if (fieldCheckList.includes(fieldProperty)) {
    if (fieldValue !== '') {
      if (fieldValue !== '0' && fieldValue !== '1') {
        frappe.throw(__(`Field Value for '${fieldProperty}' should be 0 or 1`));
      }
    }
  }
}

function fieldtypeValidation(fieldProperty, fieldValue) {
  if (fieldProperty === 'fieldtype') {
    if (fieldValue !== '') {
      if (!fieldTypeList.includes(fieldValue)) {
        frappe.throw(
          __(`Enter the correct Field Value for '${fieldProperty}'`),
        );
      }
    }
  }
}

function fieldnameValidation(fieldProperty, fieldValue) {
  if (fieldProperty === 'fieldname') {
    if (fieldValue !== '') {
      if (!/^[a-z0-9_]+$/.test(fieldValue)) {
        frappe.throw(
          __(`Enter the correct Field Value for '${fieldProperty}'`),
        );
      }
    }
  }
}

function docfieldMasterValidations(frm) {
  const mandatoryFields = ['fieldname', 'fieldtype'];
  for (const field of mandatoryFields) {
    const isValid = frm.doc.doctype_element.find(
      e => e.field_property === field,
    );
    if (!isValid) {
      frappe.throw(__(`'${field}' is required`));
    }
  }
}

frappe.ui.form.on('Docfield Master', {
  onload: frm => {
    frappe.custom_provider.set_fieldname_select(frm);
  },

  validate: frm => {
    docfieldMasterValidations(frm);

    frm.doc.element_name = frm.doc.doctype_element.find(
      i => i.field_property === 'fieldname',
    )?.field_value;

    const docfieldMasterList = {};
    for (let i = 0; i < frm.doc.doctype_element.length; i++) {
      if (Object.keys(docfieldMasterList).length !== 0) {
        if (
          Object.keys(docfieldMasterList).includes(
            frm.doc.doctype_element[i].field_property,
          )
        ) {
          frappe.throw(
            __(
              `Duplicate entry at row ${
                parseInt(i, 10) + 1
              } for Field Property value '${
                frm.doc.doctype_element[i].field_property
              }'`,
            ),
          );
        }
      }
      docfieldMasterList[frm.doc.doctype_element[i].field_property] =
        frm.doc.doctype_element[i].field_value;
    }

    for (const [key, element] of Object.entries(docfieldMasterList)) {
      checkBoxValidation(key, element);

      fieldtypeValidation(key, element);

      fieldnameValidation(key, element);
    }
  },
});

frappe.ui.form.on('Doctype Element', {
  field_value(frm, cdt, cdn) {
    const child = locals[cdt][cdn];

    checkBoxValidation(child.field_property, child.field_value);

    fieldtypeValidation(child.field_property, child.field_value);

    fieldnameValidation(child.field_property, child.field_value);
  },
});
