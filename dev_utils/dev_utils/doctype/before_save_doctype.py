import frappe
from frappe import _


def before_save_doctype(self):

    # If created for first time
    if self.get("__islocal"):
        return

    draft = frappe.db.get_list(
        "DocType Editor", filters={"docstatus": 0}, fields=["from_doctype"]
    )

    exist = next(
        (item for item in draft if item.get("from_doctype") == self.name), None
    )

    if exist:
        frappe.throw(
            _(f"DocType <b>{self.name}</b> already in draft in DocType Editor")
        )
