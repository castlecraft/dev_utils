// Copyright (c) 2023, prafful@castlecraft.in and contributors
// For license information, please see license.txt
/* global $, frappe */
function updateExcludeUpdateGrid(frm, fields) {
  const excludeFields = fields.map(d => ({
    label: `${d.label} (${d.fieldtype})`,
    value: d.fieldname,
  }));
  frm
    .get_field('exclude_update')
    .grid.update_docfield_property('field_name', 'options', excludeFields);
}

function updateExcludeImportGrid(frm, fields) {
  const excludeImport = fields
    .filter(d => d.reqd === 0)
    .map(d => ({
      label: `${d.label} (${d.fieldtype})`,
      value: d.fieldname,
    }));
  frm
    .get_field('exclude_import')
    .grid.update_docfield_property('field_name', 'options', excludeImport);
}

function getAppNames(frm) {
  frappe
    .xcall('frappe.core.doctype.module_def.module_def.get_installed_apps')
    .then(r => {
      frm.set_df_property(
        'exported_app',
        'options',
        JSON.parse(r).filter(
          e => !['frappe', 'frappe_utils', 'dev_utils'].includes(e),
        ),
      );
    });
}

frappe.meta_fields = {
  set_fieldname_select: frm => {
    if (frm.doc.meta_doctype) {
      frappe.model.with_doctype(frm.doc.meta_doctype, () => {
        const fields = $.map(
          frappe.get_doc('DocType', frm.doc.meta_doctype).fields,
          d => {
            if (
              frappe.model.no_value_type.includes(d.fieldtype) &&
              !frappe.model.table_fields.includes(d.fieldtype)
            ) {
              return null;
            } else {
              return d;
            }
          },
        );

        // Update exclude_update grid
        updateExcludeUpdateGrid(frm, fields);

        // Update exclude_import grid
        updateExcludeImportGrid(frm, fields);
      });
    }
  },
};

frappe.ui.form.on('Master Meta', {
  onload: frm => {
    frappe.meta_fields.set_fieldname_select(frm);
    frm.set_query('meta_doctype', () => {
      return {
        filters: {
          istable: 0,
        },
      };
    });
    frm.set_query('exported_app', () => {
      return {
        filters: [['exported_app', 'not in', 'frappe,frappe_utils,dev_utils']],
      };
    });
  },

  before_save: frm => {
    // when you edit and save existing master meta (eg. auto sync fixture)
    // skip setting value in exported_data_filters
    // if exported data filters is already set
    if (frm.doc.exported_data_filters) {
      return;
    }

    const filters = get_exported_data_filters(frm);
    frm.set_value('exported_data_filters', { filters });
  },

  meta_doctype(frm) {
    frm.doc.meta_fields = [];
    frm.refresh_fields();
    frappe.meta_fields.set_fieldname_select(frm);
    if (frm.doc.meta_doctype) {
      const doctypeMeta = frappe.get_doc('DocType', frm.doc.meta_doctype);
      if (!doctypeMeta) return;

      const filters = { name: doctypeMeta.module };
      const args = {
        method: 'frappe.client.get',
        args: {
          doctype: 'Module Def',
          filters,
        },
      };
      frappe.call(args).then(res => {
        frm.set_value('doctype_owner', res.message.app_name);
      });
    }

    const doctype = frm.doc.meta_doctype;
    if (doctype) {
      frappe.model.with_doctype(doctype, () => set_field_options(frm));
    } else {
      reset_filter_and_field(frm);
    }
  },

  refresh(frm) {
    frm.trigger('add_sync_button');
    hasActiveAutoSyncFixture(frm);
    getAppNames(frm);
  },

  auto_sync_fixture(frm) {
    hasActiveAutoSyncFixture(frm);
  },

  after_save(frm) {
    frappe.call({
      method:
        'dev_utils.dev_utils.doctype.master_meta.master_meta.get_linked_doctypes',
      args: {
        doctype: frm.doc.name,
      },
      type: 'GET',
      callback(res) {
        const linkedDoctypes = res.message;

        if (linkedDoctypes.length > 0) {
          frappe.msgprint(
            __(
              `Please make sure linked doctypes <strong> ${linkedDoctypes} </strong> are also exported as master to avoid failure.`,
            ),
          );
        }
      },
    });
  },

  add_sync_button: frm => {
    if (frm.doc.__islocal) return;

    if (frm.doc.status === 'Desynced') {
      frm.add_custom_button('Sync Fixture', () => {
        frappe.call({
          method: 'dev_utils.utils.create_fixtures.create_fixtures',
          args: {
            master_meta_name: frm.doc.name,
            exported_data_filter: frm.doc.exported_data_filters,
          },
          freeze: true,
          callback: r => {
            frm.reload_doc().then(x => {
              frappe.show_alert(
                {
                  message: 'Synced',
                  indicator: 'blue',
                },
                5,
              );
            });
          },
        });
      });
    }
  },
});

function hasActiveAutoSyncFixture(frm) {
  if (frm.doc.auto_sync_fixture) {
    frm.set_intro('');
    frm.set_intro(
      /* eslint-disable */
      __(
        'Making a master auto-sync makes meta syncing to happen after every change is saved on this master, this can cause performance issues hence make sure to disable auto-sync for doctypes that are frequently updated.',
        /* eslint-enable */
      ),
      'red',
    );
  } else {
    frm.set_intro('');
  }
}

/* eslint-disable */
const set_field_options = frm => {
  const filter_wrapper = frm.fields_dict.filter_list.$wrapper;
  const doctype = frm.doc.meta_doctype;

  filter_wrapper.empty();

  frm.filter_list = new frappe.ui.FilterGroup({
    parent: filter_wrapper,
    doctype: doctype,
    on_change: () => {},
  });

  frm.refresh();
};

const reset_filter_and_field = frm => {
  const filter_wrapper = frm.fields_dict.filter_list.$wrapper;
  filter_wrapper.empty();
  frm.filter_list = [];
};

const filter_fields = df => frappe.model.is_value_type(df) && !df.hidden;
const get_fields = dt => frappe.meta.get_docfields(dt).filter(filter_fields);

const get_exported_data_filters = frm => {
  let filters;
  if (frm.filter_list) {
    filters = frm.filter_list.get_filters().map(filter => filter.slice(1, 4));
  } else {
    filters = [];
  }

  return filters;
};
