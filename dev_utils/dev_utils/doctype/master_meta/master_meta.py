# Copyright (c) 2023, prafful@castlecraft.in and contributors
# For license information, please see license.txt

import json

import frappe
from frappe import _
from frappe.model.document import Document


class MasterMeta(Document):
    def autoname(self):
        if self.meta_doctype == self.doctype:
            self.name = "Master Metadata"
        else:
            self.name = self.meta_doctype

    def before_save(self):
        self.status = "Desynced"
        if (
            self.doctype_owner in ["frappe", "frappe_utils", "dev_utils"]
            and not self.exported_app
        ):
            frappe.throw(_("Exported App cannot be empty"))

        if not self.exported_app:
            self.exported_app = self.doctype_owner


@frappe.whitelist()
def get_installed_apps():
    exclude_apps = ["frappe", "frappe_utils", "dev_utils"]
    return json.dumps(
        [app for app in frappe.get_installed_apps() if app not in exclude_apps]
    )


@frappe.whitelist()
def get_linked_doctypes(doctype):
    linked_doctypes = set()

    doc_field = frappe.get_all(
        "DocField",
        filters=[["parent", "=", doctype], ["fieldtype", "=", "Link"]],
        fields=["options"],
    )

    # Filtering to get only doctype name (unique)
    linked_doctypes = {item["options"] for item in doc_field}

    # converting into list
    linked_doctypes = list(linked_doctypes)

    return linked_doctypes
