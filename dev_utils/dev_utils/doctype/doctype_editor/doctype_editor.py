# Copyright (c) 2024, prafful@castlecraft.in and contributors
# For license information, please see license.txt

import copy
import json

import frappe
from frappe.desk.form.save import savedocs
from frappe.model.document import Document
from frappe.model.naming import getseries

from dev_utils.public.js.common.variable import fieldCheckList, fieldTypeList

from dev_utils.utils.utils import (  # noqa: 501 isort: skip
    post_process_doc_json,
    stringyfy_date,
    post_process_child_tables,
)

from frappe.core.doctype.doctype.doctype import (  # isort: skip
    DocType,
    validate_fields,
    check_email_append_to,
)


class DocTypeEditor(Document):
    def validate(self):
        DocType.scrub_field_names(self)
        DocType.validate_name(self)
        check_email_append_to(self)

        validate_immutable_fields(self)
        validate_doctype_defaults(self)

    def before_save(self):
        create_doc_field_master(self)

    def on_submit(self):
        # post process args
        doc_json = self.as_dict()

        del_parent_keys = (
            "doctype_name",
            "from_doctype",
            "create_new",
            "workflow_state",
        )

        del_child_keys = ("docfield_master", "parent")

        replace_parent_keys = {
            "naming_rule_editor": "naming_rule",
            "autoname_editor": "autoname",
            "allow_rename_editor": "allow_rename",
        }

        replace_child_keys = {
            "precision_editor": "precision",
            "reference_doctype_name": "name",
        }

        defaults_parent = {
            "doctype": "DocType",
            "docstatus": 0,
            "name": self.from_doctype or self.doctype_name,
            "__islocal": 1,
        }

        defaults_child = {"parenttype": "DocType", "parent": self.from_doctype}

        post_process_doc_json(
            [doc_json],
            del_parent_keys,
            del_child_keys,
            replace_parent_keys,
            replace_child_keys,
            defaults_parent,
            defaults_child,
            default_child_delete_keys=("docstatus", "doctype", "modified"),
            common_del_keys=(),
        )

        if self.create_new:
            doc = frappe.get_doc(doc_json)
            doc.save()

        else:
            doctype = frappe.get_doc("DocType", doc_json["name"])
            doc_json["__unsaved"] = 1
            doc_json["__last_sync_on"] = stringyfy_date(doctype.modified)
            doc_json["modified"] = stringyfy_date(doctype.modified)
            doc_json["creation"] = stringyfy_date(doctype.creation)

            del doc_json["__islocal"]

            post_process_child_tables(doctype, doc_json)

            savedocs(json.dumps(doc_json), "Save")

    def autoname(self):
        prefix = (
            self.get("from_doctype")
            if self.get("from_doctype")
            else self.get("doctype_name")
        )
        prefix = prefix.strip()
        self.name = f"{prefix}-{getseries(prefix, 4)}"

    def before_insert(self):
        if self.create_new:
            return

        if frappe.db.exists(
            "DocType Editor",
            {
                "from_doctype": self.get("from_doctype"),
                "docstatus": 0,
            },
        ):
            frappe.throw(
                f"Draft of {self.get('from_doctype')} already exist"
                ", Please submit the existing one or use the same "
                "one to edit."
            )


def create_doc_field_master(self):

    for field in self.fields:
        if field.is_docfield_master:

            # Get all attributes of the field
            field_vars = vars(field)

            # Create a dictionary containing all attributes that are present in fieldTypeList    # noqa
            properties = {
                k: v for k, v in field_vars.items() if k in fieldTypeList
            }  # noqa

            existing_doc = (
                frappe.get_doc(
                    "Docfield Master",
                    f"{properties['fieldname']}",
                )
                if frappe.db.exists(
                    "Docfield Master",
                    f"{properties['fieldname']}",
                )
                else None
            )

            if existing_doc:

                # Collect updated field properties
                updated_properties = {
                    "module": self.module,
                }
                # Update existing_doc with the collected properties
                existing_doc.update(updated_properties)

                existing_fields = [
                    fields.field_property
                    for fields in existing_doc.doctype_element  # noqa: E501
                ]

                for key, value in properties.items():
                    if key not in [
                        "is_docfield_master",
                        "docfield_master",
                    ] and value not in [
                        None,
                        "",
                        0,
                    ]:
                        if key in existing_fields:
                            existing_fields_dict = {
                                field.field_property: field
                                for field in existing_doc.doctype_element
                            }
                            existing_field = existing_fields_dict.get(key)

                            if existing_field:
                                existing_field.field_value = value

                        else:
                            existing_doc.append(
                                "doctype_element",
                                {"field_property": key, "field_value": value},
                            )
                    else:
                        if key in existing_fields:
                            for field in existing_doc.doctype_element:
                                if field.field_property == key:
                                    existing_doc.doctype_element.remove(field)
                                    break
                existing_doc.save(ignore_permissions=True)

            else:
                create_docfield_master_doc = frappe.get_doc(
                    {
                        "doctype": "Docfield Master",
                        "module": self.get("module"),
                        "element_name": f"{properties['fieldname']}",  # noqa
                        "origin_doctype": self.get("from_doctype"),
                    }
                )

                for key, value in properties.items():
                    if key not in [
                        "is_docfield_master",
                        "docfield_master",
                    ] and value not in [
                        None,
                        "",
                    ]:
                        if key in fieldCheckList and value == 0:
                            continue
                        create_docfield_master_doc.append(
                            "doctype_element",
                            {"field_property": key, "field_value": value},
                        )
                create_docfield_master_doc.insert()


def validate_doctype_defaults(self):

    # Note: Creating copy of self.
    #      Parent field in self.fields is conflicting with
    #      doctype function validation,

    #      To avoid conflicts we create new object `new_doc`
    #      and set parent as doctype name

    new_doc = copy.deepcopy(self)

    new_doc.__islocal = 1
    for value in new_doc.get_all_children():
        value.__islocal = 1

    doctype_name = new_doc.get("from_doctype")

    for field in new_doc.get("fields"):
        field.parent = doctype_name

    for permission in new_doc.get("permissions"):
        permission.parent = doctype_name
    validate_fields(new_doc)


def validate_immutable_fields(self):
    for field in self.fields:
        if field.docfield_master and field.is_docfield_master == 0:
            docfield_master = frappe.get_doc(
                "Docfield Master",
                field.docfield_master,
            )
            for attribute in docfield_master.doctype_element:
                if attribute.get("immutable") == 1:
                    if (
                        getattr(field, attribute.field_property)
                        != attribute.field_value
                    ):
                        frappe.throw(
                            f"""Unable to modify <strong>'
                            {attribute.field_property}'
                              </strong> field due to its immutability
                              (found in row <strong>'{field.idx}'</strong>).
                              <br>
                                The value should remain as <strong>'
                                {attribute.field_value}'</strong>."""
                        )
