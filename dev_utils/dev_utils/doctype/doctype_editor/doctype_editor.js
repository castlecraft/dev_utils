// Copyright (c) 2024, prafful@castlecraft.in and contributors
// For license information, please see license.txt
/* global frappe, locals, __ */ /* eslint-disable */
let readOnlyFields = [];

function getSubmitedDocDetails(fromDoctype) {
  return new Promise(resolve => {
    frappe.call({
      method: 'frappe.client.get_list',
      args: {
        doctype: 'DocType Editor',
        fields: ['*'],
      },
      callback: r => {
        if (r.message) {
          let isDocDetails = false;
          for (const value of r.message) {
            if (value.from_doctype === fromDoctype && value.docstatus === 1) {
              isDocDetails = true;
              break;
            }
          }

          if (isDocDetails) {
            frappe.call({
              method: 'frappe.client.get',
              args: {
                doctype: 'DocType Editor',
                filters: {
                  from_doctype: fromDoctype,
                  docstatus: 1,
                },
                order_by: 'modified desc',
              },
              callback: r => {
                if (r.message) {
                  resolve(r.message);
                }
              },
            });
          } else {
            resolve(undefined);
          }
        }
      },
    });
  });
}

function makeFieldReadOnly(frm, readOnly) {
  if (frm.doc.__islocal === 1) {
    readOnlyFields = [];
    frm.fields.forEach(function (field) {
      const fieldToExclude = ['doctype_name', 'create_new', 'from_doctype'];
      if (!fieldToExclude.includes(field.df.fieldname) && !field.df.reqd) {
        if (field.df.read_only) {
          readOnlyFields.push(field.df.fieldname);
        }
        frm.set_df_property(field.df.fieldname, 'read_only', readOnly);
      }
    });
  }
}

frappe.ui.form.on('DocType Editor', {
  before_save: function (frm) {
    if (frm.is_new()) return;

    let custom_form_builder = frappe.custom_form_builder;
    if (custom_form_builder?.store) {
      let fields = custom_form_builder.store.update_fields();

      // if fields is a string, it means there is an error
      if (typeof fields === 'string') {
        frappe.throw(fields);
      }
    }
  },

  after_save: function (frm) {
    for (const fieldValue of Object.values(frm.doc.fields)) {
      if (fieldValue.is_docfield_master) {
        fieldValue.docfield_master = fieldValue.fieldname;
      }
    }

    if (frm.doc.__islocal === undefined) {
      frm.fields.forEach(function (field) {
        if (!readOnlyFields.includes(field.df.fieldname)) {
          frm.set_df_property(field.df.fieldname, 'read_only', false);
        }
      });
    }

    if (
      frappe.custom_form_builder &&
      frappe.custom_form_builder.doctype ===
        (frm.doc.from_doctype || frm.doc.doctype_name) &&
      frappe.custom_form_builder.store
    ) {
      frappe.custom_form_builder.store.fetch();
    }
  },
  refresh: frm => {
    set_defaults(frm);
    if (frm.is_new()) {
      frm.set_intro(
        `Please ensure to save any doctype before performing actual edit's,
        this is necessary to ensure history track from doctype head.`,
        'blue',
      );
    }

    if (
      frm.doc.__islocal !== 1 &&
      frm.doc.docstatus === 0 &&
      frm.doc.create_new === 0
    ) {
      frm.add_custom_button(__('Compare Changes'), () => {
        const doc = frappe.model.get_new_doc('DocType Editor Changes');
        doc.doctype_editor = frm.doc.name;
        frappe.set_route('Form', 'DocType Editor Changes', doc.name);
      });
    }

    if (!frm.is_new()) {
      render_custom_form_builder(frm);
    }
  },

  validate: frm => {
    if (frm.skip_validate) {
      frappe.validated = true;
      return;
    }
    frappe.validated = false;
    frappe.call({
      method: 'dev_utils.api.validate_doctype_editor',
      args: {
        doc: frm.doc,
      },
      freeze: true,
      callback: r => {
        // if no errors found
        if (!r.message.length) {
          frappe.validated = true;
          frm.skip_validate = true;
          frm.save();
          return;
        }

        create_confirmation_dialog(
          r.message,
          () => {
            frm.skip_validate = true;
            frm.save();
          },
          () => {
            frappe.validated = false;
          },
        );
      },
      freeze: true,
      async: false,
    });
  },

  before_workflow_action: async frm => {
    if (
      frm.selected_workflow_action === 'Reject' ||
      frm.selected_workflow_action === 'Amend Changes'
    ) {
      return;
    }

    const promise = new Promise((resolve, reject) => {
      frappe.dom.unfreeze();

      frappe.call({
        method: 'dev_utils.api.validate_doctype_editor',
        args: {
          doc: frm.doc,
        },
        callback: r => {
          // if no errors found
          if (!r.message.length) {
            resolve();
            return;
          }

          create_confirmation_dialog(
            r.message,

            () => {
              resolve();
            },
            () => {
              reject();
            },
          );
        },
        freeze: true,
        async: false,
      });
    });
    await promise.catch(() => frappe.throw());
  },

  create_new: function (frm) {
    if (frm.doc.create_new === 0) return;

    if (frm.doc.__islocal === 1) {
      frappe.dom.freeze();
      makeFieldReadOnly(frm, false);
      frm.clear_table('fields');
      frm.add_child('fields', {
        label: 'Dummy Field',
        fieldtype: 'Section Break',
      });

      frm.refresh_field('fields');
      makeFieldReadOnly(frm, true);
      frappe.dom.unfreeze();
    }

    frm.set_query('role', 'permissions', function (doc) {
      if (doc.custom && frappe.session.user !== 'Administrator') {
        return {
          query: 'frappe.core.doctype.role.role.role_query',
          filters: [['Role', 'name', '!=', 'All']],
        };
      }
    });

    if (
      frappe.session.user !== 'Administrator' ||
      !frappe.boot.developer_mode
    ) {
      if (frm.is_new()) {
        frm.set_value('custom', 1);
      }
      frm.toggle_enable('custom', 0);
      frm.toggle_enable('is_virtual', 0);
      frm.toggle_enable('beta', 0);
    }

    if (!frm.is_new() && !frm.doc.istable) {
      if (frm.doc.issingle) {
        frm.add_custom_button(__('Go to {0}', [__(frm.doc.name)]), () => {
          window.open(`/app/${frappe.router.slug(frm.doc.name)}`);
        });
      } else {
        frm.add_custom_button(__('Go to {0} List', [__(frm.doc.name)]), () => {
          window.open(`/app/${frappe.router.slug(frm.doc.name)}`);
        });
      }
    }

    if (frm.is_new()) {
      frm.events.set_default_permission(frm);
      frm.set_value('default_view', 'List');
    } else {
      frm.toggle_enable('engine', 0);
    }

    // set label for "In List View" for child tables
    frm.get_docfield('fields', 'in_list_view').label = frm.doc.istable
      ? __('In Grid View')
      : __('In List View');
  },

  istable: frm => {
    if (frm.doc.create_new === 0) return;
    if (frm.doc.istable && frm.is_new()) {
      frm.set_value('default_view', null);
    } else if (!frm.doc.istable && !frm.is_new()) {
      frm.events.set_default_permission(frm);
    }
  },

  set_default_permission: frm => {
    if (!(frm.doc.permissions && frm.doc.permissions.length)) {
      frm.add_child('permissions', { role: 'System Manager' });
    }
  },

  from_doctype: frm => {
    if (frm.doc.from_doctype) {
      // enable all fields to show child table pagination
      makeFieldReadOnly(frm, false);

      frappe.call({
        method: 'frappe.client.get',
        args: {
          doctype: 'DocType',
          name: frm.doc.from_doctype,
        },
        freeze: true,
        callback: async r => {
          // freeze ui and populate DocType Editor form
          frappe.dom.freeze();

          const doctypeDoc = r.message;

          doctypeDoc.autoname_editor = doctypeDoc.autoname;
          doctypeDoc.naming_rule_editor = doctypeDoc.naming_rule;
          doctypeDoc.allow_rename_editor = doctypeDoc.allow_rename;

          delete doctypeDoc.autoname;
          delete doctypeDoc.naming_rule;
          delete doctypeDoc.allow_rename;

          const submitedDocDetails = await getSubmitedDocDetails(
            frm.doc.from_doctype,
          );
          if (
            submitedDocDetails !== undefined &&
            Array.isArray(submitedDocDetails.fields)
          ) {
            doctypeDoc.fields = doctypeDoc.fields.map(field => {
              let existing_field = submitedDocDetails.fields.find(
                i => i.idx === field.idx,
              );
              if (!existing_field) return field;
              return {
                ...field,
                docfield_master: existing_field.docfield_master,
                is_docfield_master: existing_field.is_docfield_master,
              };
            });
          }
          doctypeDoc.fields = doctypeDoc.fields.map(field => {
            const precision = field.precision;

            delete field.precision;
            return {
              ...field,
              precision_editor: precision,
              parent: frm.doc.name,
              parentfield: 'fields',
              parenttype: 'DocType Editor',
              doctype: 'DocField Editor',
              reference_doctype_name: field.name,
            };
          });

          doctypeDoc.permissions = doctypeDoc.permissions.map(permission => {
            return {
              ...permission,
              parentfield: 'permissions',
              parenttype: 'DocType Editor',
              doctype: 'Permission Editor',
              reference_doctype_name: permission.name,
            };
          });

          doctypeDoc.actions = doctypeDoc.actions.map(action => {
            return {
              ...action,
              doctype: 'Action Editor',
              reference_doctype_name: action.name,
            };
          });

          doctypeDoc.links = doctypeDoc.links.map(link => {
            return {
              ...link,
              doctype: 'Link Editor',
              reference_doctype_name: link.name,
            };
          });

          doctypeDoc.states = doctypeDoc.states.map(state => {
            return {
              ...state,
              doctype: 'State Editor',
              reference_doctype_name: state.name,
            };
          });

          frm.refresh_fields();
          frm.refresh_field('fields');
          frm.set_value(doctypeDoc).then(() => {
            makeFieldReadOnly(frm, true);
            frm.refresh_field('fields');
            frm.refresh_fields();
          });

          frappe.dom.unfreeze();
        },
      });
    }
  },
});

frappe.ui.form.on('DocField Editor', {
  form_render(frm, doctype, docname) {
    // Render two select fields for Fetch From instead of Small Text for better UX
    let field = frm.cur_grid.grid_form.fields_dict.fetch_from;
    $(field.input_area).hide();

    let $doctype_select = $(`<select class="form-control">`);
    let $field_select = $(`<select class="form-control">`);
    let $wrapper = $('<div class="fetch-from-select row"><div>');
    $wrapper.append($doctype_select, $field_select);
    field.$input_wrapper.append($wrapper);
    $doctype_select.wrap('<div class="col"></div>');
    $field_select.wrap('<div class="col"></div>');

    let row = frappe.get_doc(doctype, docname);
    let curr_value = { doctype: null, fieldname: null };
    if (row.fetch_from) {
      let [doctype, fieldname] = row.fetch_from.split('.');
      curr_value.doctype = doctype;
      curr_value.fieldname = fieldname;
    }

    let doctypes = frm.doc.fields
      .filter(df => df.fieldtype == 'Link')
      .filter(df => df.options && df.fieldname != row.fieldname)
      .sort((a, b) => a.options.localeCompare(b.options))
      .map(df => ({
        label: `${df.options} (${df.fieldname})`,
        value: df.fieldname,
      }));
    $doctype_select.add_options([
      { label: __('Select DocType'), value: '', selected: true },
      ...doctypes,
    ]);

    $doctype_select.on('change', () => {
      row.fetch_from = '';
      frm.dirty();
      update_fieldname_options();
    });

    function update_fieldname_options() {
      $field_select.find('option').remove();

      let link_fieldname = $doctype_select.val();
      if (!link_fieldname) return;
      let link_field = frm.doc.fields.find(
        df => df.fieldname === link_fieldname,
      );
      let link_doctype = link_field.options;
      frappe.model.with_doctype(link_doctype, () => {
        let fields = frappe.meta
          .get_docfields(link_doctype, null, {
            fieldtype: ['not in', frappe.model.no_value_type],
          })
          .sort((a, b) => a.label.localeCompare(b.label))
          .map(df => ({
            label: `${df.label} (${df.fieldtype})`,
            value: df.fieldname,
          }));
        $field_select.add_options([
          {
            label: __('Select Field'),
            value: '',
            selected: true,
            disabled: true,
          },
          ...fields,
        ]);

        if (curr_value.fieldname) {
          $field_select.val(curr_value.fieldname);
        }
      });
    }

    $field_select.on('change', () => {
      let fetch_from = `${$doctype_select.val()}.${$field_select.val()}`;
      row.fetch_from = fetch_from;
      frm.dirty();
    });

    if (curr_value.doctype) {
      $doctype_select.val(curr_value.doctype);
      update_fieldname_options();
    }
  },

  fieldtype: function (frm) {
    frm.trigger('max_attachments');
  },

  fields_add: frm => {
    frm.trigger('setup_default_views');
  },

  docfield_master: function (frm, cdt, cdn) {
    const child = locals[cdt][cdn];
    frappe.call({
      method: 'frappe.client.get',
      args: {
        doctype: 'Docfield Master',
        name: child.docfield_master,
      },
      callback: function (r) {
        if (r.message.doctype_element) {
          const fields = frappe.meta.get_docfields(cdt, cdn);

          // Iterate while clearing existing values
          fields.forEach(function (field) {
            // Exclude field with name 'docfield_master'
            if (field.fieldname !== 'docfield_master') {
              frappe.model.set_value(cdt, cdn, field.fieldname, '');
            }
          });

          // Iterate through the retrieved data and set new values
          for (const element of r.message.doctype_element) {
            frappe.model.set_value(
              cdt,
              cdn,
              element.field_property,
              element.field_value,
            );
          }
        }
      },
    });
  },
});

function set_defaults(frm) {
  frm.dashboard.hide();
  frm.skip_validate = false;
}

function create_confirmation_dialog(message, cb, error) {
  let context = '';

  message.forEach(item => {
    context += `<li> ${item.message} </li><br>`;
  });

  frappe.confirm(
    `
    <ol> ${context} </ol>
     \n <br>
    <b>Are you sure you want to proceed?</b>`,
    () => cb(),
    () => error(),
  );
}

function render_custom_form_builder(frm) {
  // when same doctype is opened after form builder is initialized
  if (
    frappe.custom_form_builder &&
    frappe.custom_form_builder.doctype ===
      (frm.doc.from_doctype || frm.doc.doctype_name)
  ) {
    frappe.custom_form_builder.setup_page_actions();
    frappe.custom_form_builder.store.fetch();
    return;
  }

  // when different doctype is opened after form builder is initialized
  if (frappe.custom_form_builder) {
    frappe.custom_form_builder.wrapper = $(
      frm.fields_dict['form_builder'].wrapper,
    );
    frappe.custom_form_builder.frm = frm;
    frappe.custom_form_builder.doctype =
      frm.doc.from_doctype || frm.doc.doctype_name;
    frappe.custom_form_builder.customize = false;
    frappe.custom_form_builder.init(true);
    frappe.custom_form_builder.store.fetch();
    return;
  }

  // when doctype is opened for first time
  frappe.require('custom_form_builder.bundle.js').then(() => {
    // refresh gets triggered multiple times after save
    // check to avoid multiple form builder initialization
    if (frappe.custom_form_builder) return;

    // avoid form builder initialization on new doctype
    if (frm.is_new()) return;

    frappe.custom_form_builder = new frappe.ui.CustomFormBuilder({
      wrapper: $(frm.fields_dict['form_builder'].wrapper),
      frm: frm,
      doctype: frm.doc.from_doctype || frm.doc.doctype_name,
      customize: false,
    });
  });
}
