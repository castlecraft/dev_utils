import json

import frappe

from dev_utils.doctype_editor.validate_doctype_editor import (  # isort: skip
    ValidateDoctypeEditor,
)


@frappe.whitelist()
def validate_doctype_editor(doc):
    doc_as_dict = json.loads(doc)
    validator = ValidateDoctypeEditor(doc_as_dict)
    return validator.error
