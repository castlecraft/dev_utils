import frappe


def execute():
    # Action Editor
    doctypeAction_data = frappe.db.sql(
        """
        SELECT *
        FROM `tabDocType Action`
        WHERE parenttype = 'DocType Editor'
    """,
        as_dict=True,
    )

    # Iterate over each row of data and insert into 'Action Editor' table
    for row in doctypeAction_data:
        # Create document object
        action_editor_doc = frappe.get_doc(
            {
                "doctype": "Action Editor",
                **row,  # Insert all fields from the 'DocType Action' row
            }
        )
        # Save the document
        action_editor_doc.insert()
    frappe.db.commit()

    # Link Editor
    doctypeLink_data = frappe.db.sql(
        """
        SELECT *
        FROM `tabDocType Link`
        WHERE parenttype = 'DocType Editor'
    """,
        as_dict=True,
    )

    # Iterate over each row of data and insert into 'Link Editor' table
    for row in doctypeLink_data:
        # Create document object
        link_editor_doc = frappe.get_doc(
            {
                "doctype": "Link Editor",
                **row,  # Insert all fields from the 'DocType Link' row
            }
        )
        # Save the document
        link_editor_doc.insert()
    frappe.db.commit()

    # State Editor
    doctypeState_data = frappe.db.sql(
        """
        SELECT *
        FROM `tabDocType State`
        WHERE parenttype = 'DocType Editor'
    """,
        as_dict=True,
    )

    # Iterate over each row of data and insert into 'State Editor' table
    for row in doctypeState_data:
        # Create document object
        state_editor_doc = frappe.get_doc(
            {
                "doctype": "State Editor",
                **row,  # Insert all fields from the 'DocType State' row
            }
        )
        # Save the document
        state_editor_doc.insert()
    frappe.db.commit()
