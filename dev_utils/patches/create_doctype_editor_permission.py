import frappe


def execute():
    docperm_data = frappe.db.sql(
        """
        SELECT *
        FROM `tabDocPerm`
        WHERE parenttype = 'DocType Editor'
    """,
        as_dict=True,
    )

    # Iterate over each row of data and insert into Permission Editor table
    for row in docperm_data:
        # Create document object
        permission_editor_doc = frappe.get_doc(
            {
                "doctype": "Permission Editor",
                **row,  # Insert all fields from the DocPerm row
            }
        )
        # Save the document
        permission_editor_doc.insert()
    frappe.db.commit()
