/* global frappe, locals */
frappe.ui.form.on('DocField', {
  data_element(frm, cdt, cdn) {
    const child = locals[cdt][cdn];

    frappe.call({
      method: 'frappe.client.get',
      args: {
        doctype: 'Data Element',
        name: child.data_element,
      },
      callback(r) {
        if (r.message.doctype_element) {
          const fields = frappe.meta.get_docfields(cdt, cdn);

          // Iterate while clearing existing values
          fields.forEach(function (field) {
            // Exclude field with name 'data_element'
            if (field.fieldname !== 'data_element') {
              frappe.model.set_value(cdt, cdn, field.fieldname, '');
            }
          });

          // Iterate through the retrieved data and set new values
          for (const element of r.message.doctype_element) {
            frappe.model.set_value(
              cdt,
              cdn,
              element.field_property,
              element.field_value,
            );
          }
        }
      },
    });
  },
});

frappe.ui.form.on('DocType', {
  refresh(frm) {
    frm.add_custom_button(__('Edit DocType'), function () {
      const doc = frappe.model.get_new_doc('DocType Editor');
      doc.from_doctype = frm.doc.name;

      frappe.set_route('Form', 'DocType Editor', doc.name);
    });
  },
});
