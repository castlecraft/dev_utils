import unittest

import frappe
from frappe.desk.form.save import savedocs
from frappe.model.workflow import apply_workflow

from dev_utils.test.constants.doctype import CUSTOMER_DOCTYPE
from dev_utils.test.helper.frontend_mapper import doctype_editor_mapper

from dev_utils.patches.create_doctype_editor_workflow import (  # isort: skip
    create_workflow,
)

from dev_utils.test.constants.doctype_editor import (  # isort: skip
    BASE_DOCTYPE_EDITOR,
)

# List to store the records and doctypes to be deleted
# Sequence is IMPORTANT
tear_down_data = {
    "User": [],
    "DocType Editor": [],
    "DocType": [],
}


def set_up_doctype():
    # Customer doctype
    customer_doc = frappe.get_doc(CUSTOMER_DOCTYPE)
    customer_doc.name = "Custom Customer"

    customer_doc.insert()
    tear_down_data["DocType"].append(customer_doc.name)


def create_user(name, email):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": [{"role": "System Manager"}],
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        tear_down_data["User"].append(user_doc.email)


def create_doctype_editor(doc):
    mapped_doc = doctype_editor_mapper(doc)
    mapped_doc["doctype"] = "DocType Editor"
    mapped_doc["from_doctype"] = "Custom Customer"

    new_doc = frappe.get_doc(mapped_doc)
    new_doc.insert()
    tear_down_data["DocType Editor"].append(new_doc.name)
    frappe.db.commit()


def edit_draft_doctype_editor(doc, fields):
    new_doc = frappe.get_doc("DocType Editor", doc)
    new_doc.append("fields", fields)
    new_doc.save()
    frappe.db.commit()


def approve_doctype_editor(doc_name):
    doc = frappe.get_doc("DocType Editor", doc_name)
    apply_workflow(doc, "Send For Approval")
    frappe.db.commit()

    apply_workflow(doc, "Approve")
    frappe.db.commit()


class TestDoctypeEditor(unittest.TestCase):
    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")
    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def setUp(self):
        frappe.set_user("Administrator")
        create_workflow()
        set_up_doctype()

    def tearDown(self):
        # DB Cleaning goes here....
        frappe.set_user("Administrator")

        # Remove all other doctypes
        for key, array in tear_down_data.items():
            for element in array:
                frappe.delete_doc(key, element)

    def test_edit_doctype(self):
        frappe.set_user("Administrator")
        create_doctype_editor(
            "Custom Customer",
        )

        edit_draft_doctype_editor(
            tear_down_data["DocType Editor"][1],
            fields={
                "label": "City",
                "fieldname": "city",
                "fieldtype": "Data",
                "precision_editor": "",
            },
        )

        approve_doctype_editor(tear_down_data["DocType Editor"][1])

        customer_doc = frappe.get_doc("DocType", "Custom Customer")

        customer_doc_field = next(
            (field for field in customer_doc.fields if field.idx == 4), None
        )

        doctype_editor_customer_doc = frappe.get_doc(
            "DocType Editor", tear_down_data["DocType Editor"][1]
        )

        customer_doc_editor_field = next(
            (
                field
                for field in doctype_editor_customer_doc.fields
                if field.idx == 4  # noqa: E501
            ),
            None,
        )

        # Comparing Two fields
        self.assertEqual(
            customer_doc_field.get("fieldname"),
            customer_doc_editor_field.get("fieldname"),
            "doctype editor did not work as expected.",
        )

        # Cancel doc for teardown
        savedocs(doctype_editor_customer_doc.as_json(), "Cancel")

    def test_doctype_creation(self):

        doc = frappe.get_doc(BASE_DOCTYPE_EDITOR).insert()
        frappe.db.commit()
        tear_down_data["DocType Editor"].append(doc.name)  # noqa: E501
        tear_down_data["DocType"].append("Custom Contact")

        approve_doctype_editor(doc.name)

        custom_contact_doc = frappe.get_doc("DocType", "Custom Contact")

        custom_contact_fields = [
            {
                "idx": field.idx,
                "label": field.label,
                "fieldtype": field.fieldtype,
            }
            for field in custom_contact_doc.get("fields")
        ]

        custom_contact_doctype_editor_fields = [
            {
                "idx": field.idx,
                "label": field.label,
                "fieldtype": field.fieldtype,
            }
            for field in doc.get("fields")
        ]

        # Comparing Two fields
        self.assertEqual(
            custom_contact_fields,
            custom_contact_doctype_editor_fields,
            "doctype editor did not work as expected.",
        )

        # Canceling doc for teardown
        savedocs(
            frappe.get_doc("DocType Editor", doc.name).as_json(),
            "Cancel",
        )
