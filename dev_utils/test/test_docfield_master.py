import unittest

import frappe
from frappe.desk.form.save import savedocs
from frappe.model.workflow import apply_workflow

from dev_utils.test.constants.doctype import CUSTOMER_DOCTYPE
from dev_utils.test.helper.frontend_mapper import doctype_editor_mapper

from dev_utils.patches.create_doctype_editor_workflow import (  # isort: skip
    create_workflow,
)

# List to store the records and doctypes to be deleted
# Sequence is IMPORTANT
tear_down_data = {
    "User": [],
    "DocType Editor": [],
    "DocType": [],
    "Docfield Master": [],
}


def set_up_doctype():
    # Customer doctype
    customer_doc = frappe.get_doc(CUSTOMER_DOCTYPE)
    customer_doc.name = "Custom Customer"

    customer_doc.insert()
    tear_down_data["DocType"].append(customer_doc.name)


def create_user(name, email):
    user_doc = frappe.get_doc(
        {
            "doctype": "User",
            "first_name": name,
            "email": email,
            "roles": [{"role": "System Manager"}],
        }
    )
    user_doc.insert()
    if user_doc.first_name != "Anonymous":
        # Do not delete the Dummy User
        tear_down_data["User"].append(user_doc.email)


def create_doctype_editor(doc):
    mapped_doc = doctype_editor_mapper(doc)
    mapped_doc["doctype"] = "DocType Editor"
    mapped_doc["from_doctype"] = "Custom Customer"

    new_doc = frappe.get_doc(mapped_doc)
    new_doc.insert()
    tear_down_data["DocType Editor"].append(new_doc.name)
    frappe.db.commit()


def edit_draft_doctype_editor(doc, fields):
    new_doc = frappe.get_doc("DocType Editor", doc)
    new_doc.append("fields", fields)
    new_doc.save()
    frappe.db.commit()
    return fields


def setup_docfield_master(doc):
    new_doc = frappe.get_doc("Docfield Master", "city")
    tear_down_data["Docfield Master"].append(new_doc.name)
    return new_doc


def approve_doctype_editor(doc_name):
    doc = frappe.get_doc("DocType Editor", doc_name)
    apply_workflow(doc, "Send For Approval")
    frappe.db.commit()

    apply_workflow(doc, "Approve")
    frappe.db.commit()


class TestDoctypeEditor(unittest.TestCase):
    frappe.set_user("Administrator")
    exists = frappe.db.exists("User", "anonymous@gmail.com")
    if not exists:
        # Create a Dummy User with System Manager rights
        # if not present already and forget it
        create_user("Anonymous", "anonymous@gmail.com")

    def setUp(self):
        frappe.set_user("Administrator")
        create_workflow()
        set_up_doctype()

    def tearDown(self):
        # DB Cleaning goes here....
        frappe.set_user("Administrator")

        # Remove all other doctypes
        for key, array in tear_down_data.items():
            for element in array:
                frappe.delete_doc(key, element)

    def test_creation_docfield_master(self):
        frappe.set_user("Administrator")
        create_doctype_editor(
            "Custom Customer",
        )

        doctype_editor_field = edit_draft_doctype_editor(
            tear_down_data["DocType Editor"],
            fields={
                "label": "City",
                "fieldname": "city",
                "fieldtype": "Data",
                "precision_editor": "",
                "is_docfield_master": 1,
            },
        )

        approve_doctype_editor(tear_down_data["DocType Editor"])

        docfield_master = setup_docfield_master(
            tear_down_data["DocType Editor"]
        )  # noqa: E501

        doctype_editor_customer_doc = frappe.get_doc(
            "DocType Editor", tear_down_data["DocType Editor"]
        )

        self.assertIsNotNone(docfield_master, "Docfield Master not created.")

        self.assertEqual(
            docfield_master.element_name,
            "city",
            "Element name does not match.",  # noqa: E501
        )
        self.assertEqual(
            docfield_master.doctype,  # noqa: E501
            "Docfield Master",
            "Doctype does not match.",
        )

        self.assertEqual(
            docfield_master.doctype_element[0].field_value,
            doctype_editor_field["label"],
            "Label does not match",
        )

        self.assertEqual(
            docfield_master.doctype_element[1].field_value,
            doctype_editor_field["fieldname"],
            "Field Name does not match",
        )

        self.assertEqual(
            docfield_master.doctype_element[2].field_value,
            doctype_editor_field["fieldtype"],
            "Field Type does not match",
        )

        # Cancel doc for teardown
        savedocs(doctype_editor_customer_doc.as_json(), "Cancel")
