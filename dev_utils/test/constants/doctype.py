CUSTOMER_DOCTYPE = {
    "doctype": "DocType",
    "module": "Dev Utils",
    "description": "This is mock description of customer doctype.",
    "documentation": "http://mockURL.com/",
    "naming_rule": "Expression",
    "autoname": "format:{name1}",
    "fields": [
        {
            "fieldname": "name1",
            "label": "Name",
            "fieldtype": "Data",
            "reqd": 1,
        },
        {
            "fieldname": "age",
            "label": "Age",
            "fieldtype": "Int",
            "set_only_once": 1,
        },
        {
            "fieldname": "address",
            "label": "Address",
            "fieldtype": "Link",
            "options": "Address",
        },
    ],
    "permissions": [
        {
            "role": "Sales Manager",
            "read": 1,
            "write": 0,
            "create": 0,
            "delete": 0,
        },
        {
            "role": "System Manager",
            "read": 1,
            "write": 1,
            "create": 1,
            "delete": 1,
        },
    ],
}
