import frappe


def doctype_editor_mapper(doc_name):
    doc = frappe.get_doc("DocType", doc_name)
    doc_json = doc.as_dict()

    doc_json["autoname_editor"] = doc.get("autoname")
    doc_json["naming_rule_editor"] = doc.get("naming_rule")
    doc_json["allow_rename_editor"] = doc.get("allow_rename")

    del doc_json.autoname
    del doc_json.naming_rule
    del doc_json.allow_rename

    for field in doc_json.get("fields"):

        field["precision_editor"] = field.precision
        field.parent = "DocType Editor"
        field.parentfield = "fields"
        field.parenttype = "DocType"
        field.doctype = "DocField Editor"
        field["reference_doctype_name"] = field.name

        del field.precision

    return doc_json
