def after_install():
    from dev_utils.patches.create_doctype_editor_workflow import (  # noqa: E501 # isort: skip
        create_workflow,
    )

    create_workflow()
