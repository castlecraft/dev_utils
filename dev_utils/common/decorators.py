from dev_utils.common.pre_hook_validate import should_skip_hook


def exclude_doc_event_hooks(in_migrate=True):
    """use this only in case where you want to exclude frappe_utils"""

    def decorator(func):
        def wrapper(self, method=None):
            if in_migrate and should_skip_hook():
                return True

            return func(self, method)

        return wrapper

    return decorator
