import frappe


def should_skip_hook():
    if frappe.flags.in_migrate:
        return True

    if frappe.flags.in_import:
        return True
