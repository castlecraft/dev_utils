def is_singular(engine, word):
    singular = engine.singular_noun(word)
    if (
        singular is False
    ):  # inflect library returns False for both plural and irregular singular
        return True
    else:
        return engine.plural(word) == word
