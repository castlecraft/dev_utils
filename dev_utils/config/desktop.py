from frappe import _


def get_data():
    return [
        {"module_name": "Dev Utils", "type": "module", "label": _("Dev Utils")}
    ]  # noqa: 501
