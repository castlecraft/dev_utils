from setuptools import find_packages, setup

with open("requirements.txt") as f:
    install_requires = f.read().strip().split("\n")

# get version from __version__ variable in dev_utils/__init__.py
from dev_utils import __version__ as version

setup(
    name="dev_utils",
    version=version,
    description="Dev utilities and helpers",
    author="Prafful Suthar",
    author_email="prafful@castlecraft.in",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)
